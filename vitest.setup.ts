const crypto = {
  subtle: {
    importKey: () => Promise.resolve(''),
    sign: () => Promise.resolve('')
  }
};

Object.defineProperty(window, 'crypto', {
  value: crypto,
  writable: true
});
