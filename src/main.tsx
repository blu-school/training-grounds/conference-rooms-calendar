import React from 'react';
import ReactDOM from 'react-dom';
import { PublicClientApplication } from '@azure/msal-browser';
import { MsalProvider } from '@azure/msal-react';

import './styles/theme.css';
import './styles/main.css';
import App from './App';
import { msalConfig } from './config/authConfig';
import { UserContextProvider } from './store/UserContext';
import { ThemeContextProvider } from './store/ThemeContext';

const msalInstance = new PublicClientApplication(msalConfig);

ReactDOM.render(
  <React.StrictMode>
    <MsalProvider instance={msalInstance}>
      <ThemeContextProvider>
        <UserContextProvider>
          <App />
        </UserContextProvider>
      </ThemeContextProvider>
    </MsalProvider>
  </React.StrictMode>,
  document.getElementById('root')
);
