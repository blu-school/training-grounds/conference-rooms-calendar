import { setupServer } from 'msw/node';
import roomEventsEndpoints from './handlers';

const server = setupServer(...roomEventsEndpoints);

export default server;
