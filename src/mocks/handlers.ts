import { rest } from 'msw';
import roomEventsData from './roomEvent.json';

export default [
  rest.get(
    `https://graph.microsoft.com/v1.0/users/bottleneck@blubito.com/calendar/calendarView`,
    (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(roomEventsData));
    }
  ),
  rest.get('https://login.microsoftonline.com/common/discovery/instance', (req, res, ctx) => {
    return res(
      ctx.status(200),
      ctx.json({
        access_token: 'a',
        client_info: 'a',
        expires_in: 3632,
        ext_expires_in: 3632,
        id_token: 'a',
        refresh_token: 'a',
        scope:
          'Calendars.Read Calendars.Read.Shared Calendars.ReadWrite Calendars.ReadWrite.Shared openid profile User.Read email',
        token_type: 'Bearer'
      })
    );
  })
];
