import { IPublicClientApplication, AccountInfo } from '@azure/msal-browser';
import { AxiosResponse } from 'axios';
import { Event, User } from '@microsoft/microsoft-graph-types';

import { loginRequest } from '../config/authConfig';
import callMSGraphAPIEndpoint from '../config/axiosConfig';
import { IUserContext } from '../store/UserContext';

interface IGetMSGraphDataResponse extends User {
  value?: Event[];
  isError?: boolean;
}

/**
 * A wrapper for calling MS Graph with Auth Token
 * @function
 * @param {IPublicClientApplication} MSAL instance
 * @param {AccountInfo[]} MSAL accounts
 * @param {IUserContext} React.Context user context
 * @param {string} Graph endpoint
 */
export async function loadMSData(
  instance: IPublicClientApplication,
  accounts: AccountInfo[],
  userCtx: IUserContext,
  url: string
): Promise<IGetMSGraphDataResponse> {
  let accessToken = userCtx.accessToken;

  // if there is no active access token,
  // silently acquires one and updates it in the user context
  if (!accessToken) {
    const request = {
      ...loginRequest,
      account: accounts[0]
    };
    accessToken = (await instance.acquireTokenSilent(request)).accessToken;
    userCtx.updateAccessToken(accessToken);
    userCtx.updateUsername(accounts[0].username);
  }

  const response = await callMsGraph(url, accessToken);
  return response;
}

/**
 * Attaches a given access token to a Microsoft Graph API call and fetches calendar events or user data
 * @function
 * @param {string} Graph endpoint
 * @param {string} Graph access token
 */
async function callMsGraph(url: string, accessToken: string): Promise<IGetMSGraphDataResponse> {
  const bearer = `Bearer ${accessToken}`;
  const headers = {
    Authorization: bearer,
    Prefer: 'outlook.timezone="FLE Standard Time"'
  };

  try {
    const response = (await callMSGraphAPIEndpoint(url, {
      headers: headers
    })) as AxiosResponse<IGetMSGraphDataResponse>;

    return response.data;
  } catch (error) {
    console.error('Something went wrong while fetching MS Graph data!', '\n', error);
    return { isError: true };
  }
}
