import { IPublicClientApplication, AccountInfo } from '@azure/msal-browser';
import { AxiosResponse } from 'axios';
import { Event, User } from '@microsoft/microsoft-graph-types';
import { format, add } from 'date-fns';

import { loginRequest } from '../config/authConfig';
import callMSGraphAPIEndpoint from '../config/axiosConfig';
import { IUserContext } from '../store/UserContext';

interface ICreateMSEventResponse extends User {
  data?: Event;
  isError?: boolean;
}

interface IBookARoomParams {
  userCtx: IUserContext;
  meetingDuration: number;
  instance: IPublicClientApplication;
  accounts: AccountInfo[];
  createEventEndpoint: string;
  presentMoment: Date;
  roomName: string;
  roomLocation: string;
  roomCapacity: string;
}

interface ICreateMSCalenderEventParams {
  createEventEndpoint: string;
  accessToken: string;
  presentMoment: Date;
  meetingDuration: number;
  roomName: string;
  roomLocation: string;
  roomCapacity: string;
}

/**
 * A wrapper for creating MS Graph calendar event with Auth Token
 * @function
 */
export default async function bookARoom(params: IBookARoomParams) {
  let accessToken = params.userCtx.accessToken;

  // if there is no active access token,
  // silently acquires one and updates it in the user context
  if (!accessToken) {
    const request = {
      ...loginRequest,
      account: params.accounts[0]
    };
    accessToken = (await params.instance.acquireTokenSilent(request)).accessToken;
    params.userCtx.updateAccessToken(accessToken);
  }

  const response = await createMSCalenderEvent({ accessToken, ...params });
  return response;
}

/**
 * Attaches a given access token to a MS Graph API call and creates calendar events
 * with given start and end time
 * @function
 */
const createMSCalenderEvent = async (
  params: ICreateMSCalenderEventParams
): Promise<ICreateMSEventResponse> => {
  const bearer = `Bearer ${params.accessToken}`;
  const headers = {
    Authorization: bearer,
    Prefer: 'outlook.timezone="FLE Standard Time"',
    'Content-type': 'application/json'
  };

  const handleMeetingStartTime = (presentMoment: Date) => {
    const startTime = format(presentMoment, "yyyy-MM-dd'T'HH:mm:ss");
    return startTime;
  };

  const handleMeetingEndTime = (presentMoment: Date, meetingDuration: number) => {
    const result = add(presentMoment, { minutes: meetingDuration });
    const endTime = format(result, "yyyy-MM-dd'T'HH:mm:ss");
    return endTime;
  };

  const body = {
    subject: 'Meeting',
    body: {
      contentType: 'HTML'
    },
    start: {
      dateTime: handleMeetingStartTime(params.presentMoment),
      timeZone: 'FLE Standard Time'
    },
    end: {
      dateTime: handleMeetingEndTime(params.presentMoment, params.meetingDuration),
      timeZone: 'FLE Standard Time'
    },
    location: {
      displayName: `${params.roomName} - ${params.roomLocation} - ${params.roomCapacity}`
    },
    organizer: {
      emailAddress: {
        name: params.roomName.toUpperCase()
      }
    }
  };

  try {
    const response = (await callMSGraphAPIEndpoint(params.createEventEndpoint, {
      method: 'POST',
      headers: headers,
      data: body
    })) as AxiosResponse<ICreateMSEventResponse>;

    return response.data;
  } catch (error) {
    console.error('Something went wrong while creating MS calender event!', '\n', error);
    return { isError: true };
  }
};
