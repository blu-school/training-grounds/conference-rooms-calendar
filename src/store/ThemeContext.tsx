import React, { createContext, useState } from 'react';

interface ICtxProps {
  children: React.ReactNode;
}

enum ThemeType {
  light = 'light',
  dark = 'dark'
}

interface IThemeContext {
  theme: keyof typeof ThemeType;
  isThemeLight: boolean;
  changeTheme: () => void;
}

const ThemeContext = createContext<IThemeContext>({
  theme: ThemeType.dark,
  isThemeLight: false,
  changeTheme: () => ({})
});

let wasInitialLoaded = false;

const handleInitialState = (setTheme: (state: keyof typeof ThemeType) => void): void => {
  if (wasInitialLoaded) return;

  const persistedTheme = window.localStorage.getItem('app-theme');
  if (persistedTheme && (Object.values(ThemeType) as string[]).includes(persistedTheme)) {
    setTheme(persistedTheme as keyof typeof ThemeType);

    if (persistedTheme === ThemeType.light) {
      document.querySelector('.app')?.classList.add('theme--light');
    }
  }
  wasInitialLoaded = true;
};

export const ThemeContextProvider: React.FC<ICtxProps> = ({ children }: ICtxProps) => {
  const [theme, setTheme] = useState<keyof typeof ThemeType>(ThemeType.dark);
  const [isThemeLight, setIsThemeLight] = useState(false);

  const changeTheme = () => {
    const state = theme === ThemeType.dark ? ThemeType.light : ThemeType.dark;

    document.querySelector('.app')?.classList.toggle('theme--light');
    window.localStorage.setItem('app-theme', state);
    setTheme(state);
    setIsThemeLight(state === ThemeType.light);
  };

  handleInitialState(setTheme);

  const context: IThemeContext = {
    theme,
    isThemeLight,
    changeTheme
  };

  return <ThemeContext.Provider value={context}>{children}</ThemeContext.Provider>;
};

export default ThemeContext;
