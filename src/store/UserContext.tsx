import React, { createContext, useState } from 'react';

export interface IUserContext {
  username: string | null;
  updateUsername: (token: string | null) => void;
  accessToken: string | null;
  updateAccessToken: (token: string) => void;
}

interface IUserContextProviderProps {
  children: React.ReactNode;
}

const UserContext = createContext<IUserContext>({
  username: null,
  accessToken: null,
  updateUsername: (): void => {
    // updates the username
  },
  updateAccessToken: (): void => {
    // updates the token
  }
});

export const UserContextProvider: React.FC<IUserContextProviderProps> = ({
  children
}: IUserContextProviderProps) => {
  const [username, setUsername] = useState<string | null>(null);
  const [activeAccessToken, setActiveAccessToken] = useState<string | null>(null);

  const updateUsername = (username: string | null) => {
    setUsername(username);
  };

  const updateAccessToken = (accessToken: string) => {
    setActiveAccessToken(accessToken);
  };

  const context: IUserContext = {
    username,
    updateUsername,
    accessToken: activeAccessToken,
    updateAccessToken
  };

  return <UserContext.Provider value={context}>{children}</UserContext.Provider>;
};

export default UserContext;
