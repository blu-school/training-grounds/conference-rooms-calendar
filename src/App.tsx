import { AuthenticatedTemplate, UnauthenticatedTemplate } from '@azure/msal-react';
import SignInButton from './components/SignInButton';
import PageLayout from './components/PageLayout';
import { BrowserRouter as Router, Route, Routes, Navigate } from 'react-router-dom';
import Navigation from './components/Navigation';
import roomsConfig from './config/roomsConfig';
import logo from './blubito-logo.png';
import ThemeControl from './components/ThemeControl';

function App() {
  const routeFallback = '/' + Object.keys(roomsConfig)[0];
  return (
    <Router>
      <div className="app">
        <section className="app__content">
          <UnauthenticatedTemplate>
            <img src={logo} alt="logo" />
            <p>You are not signed in! Please sign in.</p>
            <SignInButton />
          </UnauthenticatedTemplate>

          <AuthenticatedTemplate>
            {/* <ThemeControl /> */}
            <Navigation />
            <Routes>
              <Route path="/" element={<Navigate to={routeFallback} />} />
              <Route path="/:room" element={<PageLayout />} />

              <Route path="*" element={<Navigate to={routeFallback} />} />
            </Routes>
          </AuthenticatedTemplate>
        </section>
      </div>
    </Router>
  );
}

export default App;
