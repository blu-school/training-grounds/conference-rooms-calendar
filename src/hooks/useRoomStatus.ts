import { useState } from 'react';
import { Event } from '@microsoft/microsoft-graph-types';
import {
  isWithinInterval,
  parseISO,
  isBefore,
  isAfter,
  differenceInMinutes,
  format
} from 'date-fns';

interface IUseRoomStatusParams {
  graphData: Event[] | null;
  presentMoment: Date;
}

/**
 * Given the room today's events, date and time the function returns an object whose keys allow getting and updating the ongoing event, room status & progressbar value
 * @function
 * @param graphData array of events or null
 * @param presentMoment string representation of current date and time
 */
const useRoomStatus = ({ graphData, presentMoment }: IUseRoomStatusParams) => {
  const [ongoingEvent, setOngoingEvent] = useState<Event | null>(null);
  const [progressbarValue, setProgressbarValue] = useState(100);

  /**
   * Updates the ongoing event
   * @function
   * @param events array of events
   */
  const handleOngoingEvent = (events: Event[]) => {
    const ongoingEvent =
      events
        .filter((event) =>
          isWithinInterval(presentMoment, {
            start: parseISO(event.start?.dateTime as string),
            end: parseISO(event.end?.dateTime as string)
          })
        )
        .shift() || null;
    setOngoingEvent(ongoingEvent);
  };

  /**
   * @function
   * Checks if there are upcoming events, then sorts them, calculates the time interval until the earliest
   * & returns it in format "HH:MM". If there are no upcoming events in the same day, returns "tomorrow"
   * @param returnAsNumber if true, the function returns the total minutes until the meeting as a number
   */
  const handleTimeUntilNextEvent = (returnAsNumber = false) => {
    if (!graphData || graphData.length < 1) return 'tomorrow';

    const upcomingEvents = graphData.filter((event) =>
      isBefore(presentMoment, parseISO(event.start?.dateTime as string))
    );

    if (upcomingEvents.length > 0) {
      const sortedEvents = upcomingEvents.sort((firstEvent, secondEvent): number => {
        if (
          isAfter(
            parseISO(firstEvent.start?.dateTime as string),
            parseISO(secondEvent.start?.dateTime as string)
          )
        ) {
          return 1;
        } else if (
          isBefore(
            parseISO(firstEvent.start?.dateTime as string),
            parseISO(secondEvent.start?.dateTime as string)
          )
        ) {
          return -1;
        }
        return 0;
      });

      const minutesAvailability = differenceInMinutes(
        parseISO(sortedEvents[0].start?.dateTime as string),
        presentMoment
      );
      if (returnAsNumber) return minutesAvailability;

      const formattedTime = format(parseISO(sortedEvents[0].start?.dateTime as string), 'p');
      return `${formattedTime}`;
    }
    return 'tomorrow';
  };

  /**
   * Given the total meeting time and the time until end, returns the progressbar value
   * @function
   * @param timeUntilEnd
   * @param totalMeetingTime
   */
  const handleProgressbarValue = (timeUntilEnd: number, totalMeetingTime: number) => {
    const value = 100 - Number(((100 * timeUntilEnd) / totalMeetingTime).toFixed());
    if (value < 0) {
      return 100;
    }
    return value;
  };

  /**
   * Returns the time left until event's end in format "HH:MM" and updates the progressbar value if needed
   */
  const handleTimeUntilFee = () => {
    const minutesUntilFree =
      1 + differenceInMinutes(parseISO(ongoingEvent?.end?.dateTime as string), presentMoment);
    const totalMeetingTime = differenceInMinutes(
      parseISO(ongoingEvent?.end?.dateTime as string),
      parseISO(ongoingEvent?.start?.dateTime as string)
    );
    const newValue = handleProgressbarValue(minutesUntilFree, totalMeetingTime);
    if (progressbarValue !== newValue) {
      setProgressbarValue(newValue);
    }
    return format(parseISO(ongoingEvent?.end?.dateTime as string), 'p');
  };

  return {
    ongoingEvent: {
      value: ongoingEvent,
      handleChange: handleOngoingEvent
    },
    roomAvailability: {
      handleTimeUntilNextEvent,
      handleTimeUntilFee
    },
    progressbarValue
  };
};

export default useRoomStatus;
