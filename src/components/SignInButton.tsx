import { useContext } from 'react';
import { useMsal } from '@azure/msal-react';

import { loginRequest } from '../config/authConfig';
import UserContext from '../store/UserContext';

/**
 * Renders a button which, when selected, will open a popup for login
 */
const SignInButton: React.FC = () => {
  const { instance } = useMsal();
  const userCtx = useContext(UserContext);

  const handleLogin = async () => {
    try {
      const loginResponse = await instance.loginPopup(loginRequest);
      userCtx.updateUsername(loginResponse.account?.username || null);
      userCtx.updateAccessToken(loginResponse.accessToken);
    } catch (error) {
      console.error('Login failed!', '\n', error);
    }
  };

  return (
    <button type="button" onClick={handleLogin}>
      Sign in using Popup
    </button>
  );
};
export default SignInButton;
