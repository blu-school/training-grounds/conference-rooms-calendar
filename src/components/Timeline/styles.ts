import styled from 'styled-components';
import Paper from '@mui/material/Paper';
import Dialog from '@mui/material/Dialog';

export const FlexArea = styled.div.attrs({
  className: 'flex-area'
})`
  display: flex;
  align-items: center;

  @media (max-width: 1024px) {
    width: 220px;
  }
`;

export const TimelineContainer = styled(Paper)`
  --clr-bg-neutral: var(--thm-gradient-start);
  --clr-bg-accent: var(--thm-gradient-end);
  --clr-border-divider: slategrey;

  position: relative;
  width: 250px;
  height: 557px;
  overflow: auto;
  background-color: var(--clr-bg-neutral) !important;
  box-shadow: unset !important;

  &.theme--light {
    --clr-bg-neutral: var(--thm-gradient-end);
    --clr-bg-accent: var(--thm-gradient-start);
    --clr-border-divider: var(--clr-blue-1);
  }

  .MainLayout-container {
    display: flex;
    width: inherit;
  }

  .MainLayout-container::-webkit-scrollbar {
    width: 5px;
  }

  .MainLayout-container::-webkit-scrollbar-track {
    background-color: rgba(8, 18, 28, 1);
  }

  .MainLayout-container::-webkit-scrollbar-thumb {
    background-color: slategray;
    outline: 1px solid rgba(8, 18, 28, 1);
    border-radius: 10px;
    box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.5);
    overflow: hidden;
  }

  .MuiTableRow-root:nth-child(odd) .TickCell-cell {
    border-bottom: 2px solid transparent;
  }
  .MuiTableRow-root:nth-child(even) .TickCell-cell {
    border-bottom: 2px solid var(--clr-border-divider);
  }

  .TickCell-cell {
    background: var(--clr-bg-neutral) !important;
    width: 66px;
    top: 10px;
  }

  .MuiTableCell-root .Label-emptyLabel {
    line-height: 20px;
  }

  .MuiTableCell-root .Label-emptyLabel:last-child .Label-text {
    color: transparent !important;
  }

  .MuiTableCell-root .Label-label:not(.Label-emptyLabel) {
    line-height: 5;
  }

  .MuiTableCell-root .Label-label:nth-child(even) .Label-text {
    color: transparent !important;
  }

  .Layout-timeScaleContainer {
    position: absolute;
    width: 55px !important;
  }

  .DayScaleEmptyCell-emptyCell {
    background-color: var(--clr-bg-neutral);
  }

  .MainLayout-header .MainLayout-background {
    display: none;
  }

  .MainLayout-container .MainLayout-ordinaryLeftPanelBorder {
    border-right: none;
  }

  .MainLayout-inlineFlex {
    border-top-width: 2px;
    border-top-style: solid;
    border-top-color: var(--clr-border-divider);
    border-bottom-width: 2px;
    border-bottom-style: solid;
    border-bottom-color: var(--clr-border-divider);
  }

  .Label-text {
    color: #57727f !important;
  }

  .MuiTableRow-root .MuiTableCell-root.Cell-cell {
    border-bottom: 2px solid var(--clr-border-divider);
  }

  .MuiTableRow-root .MuiTableCell-root.Cell-cell {
    background: linear-gradient(
      90deg,
      var(--clr-bg-neutral) 0%,
      var(--clr-bg-accent) 50%,
      var(--clr-bg-neutral) 100%
    ) !important;
  }

  @media (max-width: 576px) {
    /* height: 89vh; */
    width: auto;
  }
`;

export const StyledClock = styled.div<{ top?: string }>`
  width: 100%;
  height: 0.5px;
  padding: 0.5px;
  position: absolute;
  z-index: 10;
  /* margin-right: 20px; */
  text-align: center;
  /* letter-spacing: NaNpx; */
  color: #ffffff;
  font: normal normal normal 14px/17px Filson Pro;
  background-color: red;
  top: ${({ top }) => top};
`;

export const MeetingModal = styled(Dialog)`
  > div {
    > div {
      width: 660px;
      height: 600px;
      padding: 50px;
      background: var(--clr-blue-1) 0% 0% no-repeat padding-box;
      box-sizing: border-box;
      border-radius: 26px;
    }
  }
  svg {
    color: var(--clr-blue);
    font-size: 35px;
    margin-right: 15px;
  }
  span {
    color: #fff;
    font-size: 18px;
  }
`;

export const TitleAndCloseButtonWrapper = styled.div`
  display: flex;
  margin-bottom: 40px;
  > button {
    padding: 0;
  }
  > h2 {
    color: #ffcf54;
    font-size: 28px;
    flex-grow: 1;
    text-align: center;
  }
`;

export const ModalContentWrapper = styled.div`
  display: flex;
  flex-direction: column;
  > div {
    display: flex;
    align-items: center;
    justify-content: center;
    margin-bottom: 50px;
    > span:first-of-type {
      margin-right: 10px;
    }
  }
  > div:first-of-type {
    justify-content: center;
    > span:first-of-type {
      color: var(--clr-blue);
    }
  }
`;
