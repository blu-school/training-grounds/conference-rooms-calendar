import { Appointments } from '@devexpress/dx-react-scheduler-material-ui';

const CustomAppointment = (props: Appointments.AppointmentProps) => {
  const { title, author, startDate, endDate, location, setMeetingInfoModalState } = props.data;

  return (
    <Appointments.Appointment
      style={{
        backgroundColor: '#08121c',
        border: '2px solid #fdcd53',
        borderLeftWidth: '15px',
        borderRadius: '8px',
        color: '#fff',
        paddingTop: '2px'
      }}
      onClick={() =>
        setMeetingInfoModalState({ isOpen: true, title, author, startDate, endDate, location })
      }
      data={props.data}
      draggable={false}
      resources={[]}>
      <div style={{ fontSize: '10px' }}>{title}</div>
      <div style={{ color: '#57727f', fontSize: '10px' }}>{author}</div>
    </Appointments.Appointment>
  );
};

export default CustomAppointment;
