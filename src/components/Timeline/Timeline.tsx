import { useEffect, useRef, useState, useContext } from 'react';
import { format } from 'date-fns';
import { Event } from '@microsoft/microsoft-graph-types';
import { ViewState, AppointmentModel } from '@devexpress/dx-react-scheduler';
import { Scheduler, DayView, Appointments } from '@devexpress/dx-react-scheduler-material-ui';
import ThemeContext from '../../store/ThemeContext';

import Clock from './Clock';
import CustomAppointment from './CustomAppointment';
import MeetingInfoModal from './MeetingInfoModal';
import { TimelineContainer, FlexArea } from './styles';

interface ITimelineProps {
  graphData: Event[];
  presentMoment: Date;
}

export interface IMeetingInfoModalState {
  isOpen: boolean;
  title: string;
  author: string;
  startDate: string;
  endDate: string;
  location: string;
}

const Timeline: React.FC<ITimelineProps> = ({ graphData, presentMoment }: ITimelineProps) => {
  const themeCtx = useContext(ThemeContext);
  const [appointments, setAppointments] = useState<AppointmentModel[]>([]);
  const [meetingInfoModalState, setMeetingInfoModalState] = useState<IMeetingInfoModalState>({
    isOpen: false,
    title: '',
    author: '',
    startDate: '',
    endDate: '',
    location: ''
  });
  const schedulerRef = useRef<HTMLDivElement | null>(null);
  const firstCell = document
    .getElementsByClassName('Label-emptyLabel')
    .item(0) as HTMLElement | null;
  const lastCell = document
    .getElementsByClassName('Label-emptyLabel')
    .item(1) as HTMLElement | null;
  const startSpan = document.createElement('span');
  const endSpan = document.createElement('span');
  const startHour = document.getElementsByClassName('Label-text')[0]?.textContent as string;
  const endHour = document.getElementsByClassName('Label-text')[
    document.getElementsByClassName('Label-text').length - 1
  ]?.textContent as string;

  const subMinutes = (dt, h) => {
    if (h) {
      const s = h.split(/[:\s]+/);
      dt.setHours(s[2] === 'PM' ? (s[0] == 12 ? 0 : 12) + parseInt(s[0], 10) : parseInt(s[0], 10));
      dt.setMinutes(parseInt(s[1], 10) - 30);
      dt.setSeconds(0o0);
      return dt.toLocaleTimeString([], { hour: '2-digit', minute: '2-digit' });
    }
  };

  const addMinutes = (dt, h) => {
    if (h) {
      const s = h.split(/[:\s]+/);
      dt.setHours(s[2] === 'PM' ? (s[0] == 12 ? 0 : 12) + parseInt(s[0], 10) : parseInt(s[0], 10));
      dt.setMinutes(parseInt(s[1], 10) + 30);
      dt.setSeconds(0o0);
      return dt.toLocaleTimeString([], { hour: '2-digit', minute: '2-digit' });
    }
  };

  const handleAppointments = (graphData: Event[]) => {
    const appointments: AppointmentModel[] = [];

    graphData.map((event) => {
      appointments.push({
        startDate: event.start?.dateTime || '',
        endDate: event.end?.dateTime,
        title: event.subject || '',
        author: event.organizer?.emailAddress?.name,
        location: event.location?.displayName,
        setMeetingInfoModalState
      });
    });

    return appointments;
  };

  useEffect(() => {
    startSpan.classList.add('Label-text');
    startSpan.innerHTML = subMinutes(new Date(), startHour);
    firstCell?.appendChild(startSpan);
  }, [firstCell]);

  useEffect(() => {
    endSpan.classList.add('Label-text');
    endSpan.innerHTML = addMinutes(new Date(), endHour);
    lastCell?.appendChild(endSpan);
  }, [lastCell]);

  useEffect(() => {
    setAppointments(handleAppointments(graphData));
  }, [graphData]);

  return (
    <>
      <div className="theme-transition">
        <MeetingInfoModal
          meetingInfoModalState={meetingInfoModalState}
          setMeetingInfoModalState={setMeetingInfoModalState}
        />
        <FlexArea>
          <Clock schedulerRef={schedulerRef} />
          <TimelineContainer
            ref={schedulerRef}
            className={`timeline-background ${themeCtx.isThemeLight ? 'theme--light' : ''}`}>
            <Scheduler data={appointments}>
              <ViewState currentDate={format(presentMoment, 'yyyy-MM-dd')} />
              <DayView startDayHour={9} endDayHour={19} />
              <Appointments appointmentComponent={CustomAppointment} />
            </Scheduler>
          </TimelineContainer>
        </FlexArea>
      </div>
    </>
  );
};

export default Timeline;
