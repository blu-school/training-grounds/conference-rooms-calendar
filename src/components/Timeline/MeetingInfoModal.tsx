import ArrowBackRoundedIcon from '@mui/icons-material/ArrowBackRounded';
import LocationOnOutlinedIcon from '@mui/icons-material/LocationOnOutlined';
import AccessTimeIcon from '@mui/icons-material/AccessTime';
import IconButton from '@mui/material/IconButton';

import { IMeetingInfoModalState } from './Timeline';
import { MeetingModal, TitleAndCloseButtonWrapper, ModalContentWrapper } from './styles';
import { format } from 'date-fns';
import { parseISO } from 'date-fns/esm';
import { useLocation } from 'react-router-dom';
import roomsConfig from '../../config/roomsConfig';

interface IMeetingInfoModalProps {
  meetingInfoModalState: IMeetingInfoModalState;
  setMeetingInfoModalState: React.Dispatch<React.SetStateAction<IMeetingInfoModalState>>;
}

const MeetingInfoModal: React.FC<IMeetingInfoModalProps> = ({
  meetingInfoModalState,
  setMeetingInfoModalState
}: IMeetingInfoModalProps) => {
  const { isOpen, title, author, startDate, endDate, location } = meetingInfoModalState;
  const navLocation = useLocation();

  const formatTime = (stringDate: string) => {
    return format(parseISO(stringDate), 'dd.MM.yyyy p');
  };

  const resetModalState = () => {
    return {
      isOpen: false,
      title: '',
      author: '',
      startDate: '',
      endDate: '',
      location: ''
    };
  };

  const handleRoomLocaion = (location: string) => {
    if (!location) {
      const urlRoomLocation = navLocation.pathname.split('/')[1];
      const roomLocation = `${roomsConfig[urlRoomLocation].name.toUpperCase()} - ${
        roomsConfig[urlRoomLocation].location
      } - Capacity ${roomsConfig[urlRoomLocation].capacity}`;
      return roomLocation;
    }
    return location;
  };

  return (
    <MeetingModal
      open={isOpen}
      maxWidth="md"
      onClose={() => setMeetingInfoModalState(resetModalState())}>
      <div>
        <TitleAndCloseButtonWrapper>
          <IconButton onClick={() => setMeetingInfoModalState(resetModalState())}>
            <ArrowBackRoundedIcon />
          </IconButton>
          <h2>{title ? title : 'Meeting'}</h2>
        </TitleAndCloseButtonWrapper>
        <ModalContentWrapper>
          <div>
            <span>Organizer:</span>
            <span>{author}</span>
          </div>
          <div>
            <LocationOnOutlinedIcon />
            <span>{handleRoomLocaion(location)}</span>
          </div>
          <div>
            <AccessTimeIcon />
            <span>Start:</span>
            <span>{startDate && formatTime(startDate)}</span>
          </div>
          <div>
            <AccessTimeIcon />
            <span>End:</span>
            <span>{endDate && formatTime(endDate)}</span>
          </div>
        </ModalContentWrapper>
      </div>
    </MeetingModal>
  );
};

export default MeetingInfoModal;
