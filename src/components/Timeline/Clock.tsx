import { useEffect, useRef, useState } from 'react';
import { isWithinInterval } from 'date-fns';

import useInterval from '../../hooks/useInterval';
import { StyledClock } from '../Timeline/styles';

interface IClockProps {
  schedulerRef: React.MutableRefObject<HTMLDivElement | null>;
}

function Clock({ schedulerRef }: IClockProps) {
  const clock = useRef<HTMLDivElement | null>(null);
  const [clockCssTop, setClockCssTop] = useState('');
  const [isWithin, setIsWithin] = useState(false);
  const [presentMoment, setPresentMoment] = useState<Date>(new Date());
  const [scrollFlag, setScrollFlag] = useState(false);

  const setHours = (dt, h) => {
    const s = h.split(/[:\s]+/);
    dt.setHours(s[2] === 'PM' ? (s[0] == 12 ? 0 : 12) + parseInt(s[0], 10) : parseInt(s[0], 10));
    dt.setMinutes(parseInt(s[1], 10));
    dt.setSeconds(0o0);
    return dt;
  };

  useEffect(() => {
    const list = document.getElementsByClassName('Label-text');
    for (let i = 0; i + 1 < list.length; i++) {
      const startDate = setHours(new Date(), list[i].innerHTML);
      const endDate = setHours(new Date(), list[i + 1].innerHTML);
      if (
        isWithinInterval(presentMoment.setSeconds(0o0), {
          start: startDate,
          end: endDate
        })
      )
        if (schedulerRef.current) {
          setClockCssTop(`${5 + i * 8.6}%`);
          const mainContainer = document?.querySelector('.MainLayout-container');
          if (i > 9 && mainContainer && scrollFlag == false) {
            mainContainer.scrollTop = 444;
            setScrollFlag(true);
          }
          setIsWithin(true);
        }
    }
  }, [presentMoment]);

  useInterval(() => {
    setPresentMoment(new Date());
  }, 1000);

  useEffect(() => {
    if (isWithin && clock.current && schedulerRef) {
      const el = schedulerRef.current?.children[0].children[0];
      el?.prepend(clock.current);
      clock.current.setAttribute('style', 'display: block');
    } else if (clock.current) {
      document.querySelector('.flex-area')?.appendChild(clock.current);
      clock.current.setAttribute('style', 'display: none');
    }
  }, [isWithin]);

  return <StyledClock ref={clock} top={clockCssTop}></StyledClock>;
}
export default Clock;
