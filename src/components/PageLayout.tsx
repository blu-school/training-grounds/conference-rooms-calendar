import { useContext, useEffect, useState } from 'react';
import { useMsal } from '@azure/msal-react';
import { loadMSData } from '../utils/loadMSData';
import { Event } from '@microsoft/microsoft-graph-types';

import useInterval from '../hooks/useInterval';
import UserContext from '.././store/UserContext';
import RoomStatus from './RoomStatus';
import Timeline from './Timeline/Timeline';
import { useLocation } from 'react-router-dom';
import roomsConfig from '../config/roomsConfig';

const PageLayout: React.FC = () => {
  const location = useLocation();
  const roomName = location.pathname.split('/')[1];

  const { instance, accounts } = useMsal();
  const [graphData, setGraphData] = useState<Event[]>([]);
  const userCtx = useContext(UserContext);
  const [presentMoment, setPresentMoment] = useState<Date>(new Date());
  const [roomStatusLoading, setRoomStatusLoading] = useState<boolean>(true);

  const handleRoomEvents = async () => {
    const resp = await loadMSData(
      instance,
      accounts,
      userCtx,
      roomsConfig[roomName].eventsEndpoint
    );
    if (!resp.isError && resp.value) {
      setGraphData(resp.value);
    }
    setRoomStatusLoading(false);
  };

  useEffect(() => {
    handleRoomEvents();
  }, [presentMoment, location]);

  useInterval(() => {
    setPresentMoment(new Date());
  }, 60000);

  const triggerDataRefresh = async () => {
    await handleRoomEvents();
  };

  return (
    <div className="eventsWrapper">
      <RoomStatus
        roomName={roomName}
        graphData={graphData}
        presentMoment={presentMoment}
        createEventEndpoint={roomsConfig[roomName]?.createEventEndpoint}
        accounts={accounts}
        instance={instance}
        loading={roomStatusLoading}
        triggerDataRefresh={triggerDataRefresh}
      />
      <Timeline graphData={graphData} presentMoment={presentMoment} />
    </div>
  );
};

export default PageLayout;
