import * as React from 'react';
import { ThemeBtn } from './styles';
import ThemeContext from '../../store/ThemeContext';

const ThemeControl: React.VFC = () => {
  const themeCtx = React.useContext(ThemeContext);
  return (
    <ThemeBtn
      onClick={() => themeCtx.changeTheme()}
      className={`theme-transition ${themeCtx.isThemeLight ? 'theme--light' : ''}`}>
      <span className={`selector `} />
      <span>🌑</span>
      <span>☀️</span>
    </ThemeBtn>
  );
};

export default ThemeControl;
