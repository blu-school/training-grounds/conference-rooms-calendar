import styled from 'styled-components';

export const ThemeBtn = styled.button`
  --btn-clr-primary: var(--clr-blue);
  --btn-clr-secondary: var(--clr-blue-1);
  --base-size: 25px;
  position: absolute;
  top: 0.5rem;
  right: 1rem;
  cursor: pointer;
  width: 3.5rem;
  padding: 0;
  border: 2px solid var(--btn-clr-primary);
  background-color: var(--btn-clr-secondary);
  display: flex;
  justify-content: space-between;
  border-radius: 1rem;
  font-size: 1rem;
  height: var(--base-size);

  span {
    position: relative;
    top: -1px;
  }

  .selector {
    position: absolute;
    height: 100%;
    aspect-ratio: 1;
    background-color: var(--btn-clr-primary);
    border-radius: 100%;
    transform: scale(1.4);
    top: 0px;
    left: 0px;
    transition: left 300ms ease-in-out;
  }

  &.theme--light {
    --btn-clr-primary: var(--clr-blue-1);
    --btn-clr-secondary: var(--clr-blue);
    .selector {
      left: calc(100% - var(--base-size) + 2px);
    }
  }
`;
