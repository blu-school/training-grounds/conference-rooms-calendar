// import { render, screen, act } from '@testing-library/react';
// import { vi } from 'vitest';
// import {
//   AccountInfo,
//   Configuration,
//   EventCallbackFunction,
//   EventMessage,
//   EventType,
//   InteractionType,
//   PublicClientApplication
// } from '@azure/msal-browser';
// import { MsalProvider } from '@azure/msal-react';
// import { UserContextProvider } from '../../../store/UserContext';
// import server from '../../../mocks/server';
// import { testAccount, TEST_CONFIG } from '../../../mocks/testConstants';
// import RoomStatus from '../index';
// import { graphConfig } from '../../../config/authConfig';

// beforeAll(() => server.listen({ onUnhandledRequest: 'error' }));
// afterAll(() => server.close());
// afterEach(() => server.resetHandlers());

// describe('Room status tests', () => {
//   let pca: PublicClientApplication;
//   const msalConfig: Configuration = {
//     auth: {
//       clientId: TEST_CONFIG.MSAL_CLIENT_ID
//     }
//   };

//   let eventCallbacks: EventCallbackFunction[];
//   let cachedAccounts: AccountInfo[] = [];

//   beforeEach(() => {
//     eventCallbacks = [];
//     let eventId = 0;
//     pca = new PublicClientApplication(msalConfig);
//     vi.spyOn(pca, 'addEventCallback').mockImplementation((callbackFn) => {
//       eventCallbacks.push(callbackFn);
//       eventId += 1;
//       return eventId.toString();
//     });
//     vi.spyOn(pca, 'handleRedirectPromise').mockImplementation(() => {
//       const eventStart: EventMessage = {
//         eventType: EventType.HANDLE_REDIRECT_START,
//         interactionType: InteractionType.Redirect,
//         payload: null,
//         error: null,
//         timestamp: 10000
//       };

//       eventCallbacks.forEach((callback) => {
//         callback(eventStart);
//       });

//       const eventEnd: EventMessage = {
//         eventType: EventType.HANDLE_REDIRECT_END,
//         interactionType: InteractionType.Redirect,
//         payload: null,
//         error: null,
//         timestamp: 10000
//       };

//       eventCallbacks.forEach((callback) => {
//         callback(eventEnd);
//       });
//       return Promise.resolve(null);
//     });

//     vi.spyOn(pca, 'getAllAccounts').mockImplementation(() => cachedAccounts);
//   });

//   afterEach(() => {
//     // cleanup on exiting
//     vi.restoreAllMocks();
//     vi.clearAllMocks();
//     cachedAccounts = [];
//     server.resetHandlers();
//   });

//   it('should render the componet', () => {
//     const eventMessage = {
//       eventType: EventType.ACCOUNT_ADDED,
//       interactionType: InteractionType.Popup,
//       payload: null,
//       error: null,
//       timestamp: 10000
//     };
//     cachedAccounts = [testAccount];

//     act(() => {
//       eventCallbacks.forEach((callback) => {
//         callback(eventMessage);
//       });
//     });

//     render(
//       <MsalProvider instance={pca}>
//         <UserContextProvider>
//           <RoomStatus
//             eventsEndpoint={graphConfig.bottleneckEvents.eventsEndpoint}
//             createEventEndpoint={graphConfig.bottleneckEvents.createEventEndpoint}
//             roomName="bottleneck"
//             accounts={cachedAccounts}
//             instance={pca}
//           />
//         </UserContextProvider>
//       </MsalProvider>
//     );

//     console.log(screen.getByTestId('heading').textContent);
//     expect(screen.getByTestId('heading').textContent).toEqual('BOTTLENECK');
//   });
// });
