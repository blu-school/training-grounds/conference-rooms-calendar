import { useState, useEffect, useContext } from 'react';
import { format } from 'date-fns';
import { Event } from '@microsoft/microsoft-graph-types';
import { AccountInfo, IPublicClientApplication } from '@azure/msal-browser';
import ArrowBackRoundedIcon from '@mui/icons-material/ArrowBackRounded';
import LocationOnOutlinedIcon from '@mui/icons-material/LocationOnOutlined';
import AccessTimeIcon from '@mui/icons-material/AccessTime';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import MenuItem from '@mui/material/MenuItem';
import roomsConfig from '../../config/roomsConfig';
import UserContext from '../../store/UserContext';
import bookARoom from '../../utils/bookARoom';
import {
  Dialog,
  TitleAndCloseButtonWrapper,
  CloseArrowButton,
  RoomInfoWrapper,
  DateAndDropdownWrapper,
  CurrentTime,
  Dropdown,
  BookingButton
} from './styles';

interface INewMeetingDialogProps {
  modalProps: {
    isOpen: boolean;
    setIsDialogOpen: (param: boolean) => void;
    roomName: string;
    roomLocation: string;
    roomCapacity: string;
    presentMoment: Date;
    ongoingEvent: {
      value: Event | null;
      handleChange: (events: Event[]) => void;
    };
    handleTimeUntilNextEvent: (returnAsNumber: boolean) => string | number;
    instance: IPublicClientApplication;
    accounts: AccountInfo[];
    createEventEndpoint: string;
    triggerDataRefresh: () => void;
  };
}

interface IDropdownOption {
  value: number;
  text: string;
  disabled: boolean;
}

const dropdownOptions: IDropdownOption[] = [
  {
    value: 15,
    text: '15min',
    disabled: false
  },
  {
    value: 30,
    text: '30min',
    disabled: false
  },
  {
    value: 45,
    text: '45min',
    disabled: false
  },
  {
    value: 60,
    text: '60min',
    disabled: false
  }
];

const NewMeetingModal: React.FC<INewMeetingDialogProps> = ({
  modalProps
}: INewMeetingDialogProps) => {
  const userCtx = useContext(UserContext);
  const [dropdownSelectedValue, setDropdownSelectedValue] = useState<number>(15);
  const [isButtonDisabled, setIsButtonDisabled] = useState<boolean>(false);
  const currentDate = format(modalProps.presentMoment, 'dd.MM.yyyy');
  const currentTime = format(modalProps.presentMoment, 'h a');

  const handleAvailableDropdownOptions = (dropdownOptions: IDropdownOption[]) => {
    const timeUntilNextEvent = modalProps.handleTimeUntilNextEvent(true);
    if (typeof timeUntilNextEvent === 'number' && timeUntilNextEvent <= 15) {
      setIsButtonDisabled(true);
    }
    dropdownOptions.forEach((option) => {
      if (option.value >= (timeUntilNextEvent as number)) {
        option.disabled = true;
      }
    });
  };

  const handleBookingButtonClick = async () => {
    const newEvent = await bookARoom({
      userCtx,
      meetingDuration: dropdownSelectedValue,
      ...modalProps
    });
    if (!newEvent.isError) {
      modalProps.ongoingEvent.handleChange([newEvent]);
      modalProps.triggerDataRefresh();
    }
  };

  useEffect(() => {
    handleAvailableDropdownOptions(dropdownOptions);
  }, [modalProps.ongoingEvent]);

  return (
    <Dialog
      open={modalProps.isOpen}
      maxWidth="md"
      onClose={() => modalProps.setIsDialogOpen(false)}>
      <TitleAndCloseButtonWrapper>
        <CloseArrowButton onClick={() => modalProps.setIsDialogOpen(false)}>
          <ArrowBackRoundedIcon />
        </CloseArrowButton>
        <h2>New meeting</h2>
      </TitleAndCloseButtonWrapper>
      <RoomInfoWrapper>
        <LocationOnOutlinedIcon />
        <span>{modalProps.roomName}</span>
        <span>
          {/* room capacity can be passed as a prop like the room name*/}
          {modalProps.roomLocation} <span>{roomsConfig[modalProps.roomName]?.capacity}</span>
        </span>
      </RoomInfoWrapper>
      <DateAndDropdownWrapper>
        <AccessTimeIcon />
        <span>{currentDate}</span>
        <CurrentTime>{currentTime}</CurrentTime>
        <Dropdown
          value={dropdownSelectedValue}
          IconComponent={KeyboardArrowDownIcon}
          MenuProps={{
            disablePortal: true,
            anchorOrigin: {
              vertical: 'center',
              horizontal: 'center'
            },
            PaperProps: {
              style: {
                minWidth: '90px',
                marginTop: '17px',
                border: '1px solid #707070'
              }
            }
          }}
          onChange={(event) => setDropdownSelectedValue(Number(event.target.value))}>
          {dropdownOptions.map((option, index) => (
            <MenuItem key={index} value={option.value} disabled={option.disabled}>
              {option.text}
            </MenuItem>
          ))}
        </Dropdown>
      </DateAndDropdownWrapper>
      <BookingButton
        disabled={isButtonDisabled}
        modalButton
        onClick={() => {
          handleBookingButtonClick();
          modalProps.setIsDialogOpen(false);
        }}>
        <span>
          <em>b</em>ook
        </span>
        <span>now</span>
      </BookingButton>
    </Dialog>
  );
};

export default NewMeetingModal;
