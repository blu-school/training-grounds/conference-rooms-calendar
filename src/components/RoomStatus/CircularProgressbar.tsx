import { CircularProgressbarWithChildren, buildStyles } from 'react-circular-progressbar';

interface ICircularProgressbarProps {
  value: number;
}

interface IRadialSeparatorsProps {
  count: number;
  style: React.CSSProperties;
}

interface ISeparatorProps {
  turns: number;
  style: React.CSSProperties;
}

const Separator: React.FC<ISeparatorProps> = ({ turns, style }: ISeparatorProps) => {
  return (
    <div
      style={{
        position: 'absolute',
        height: '100%',
        transform: `rotate(${turns}turn)`
      }}>
      <div style={style} />
    </div>
  );
};

const RadialSeparators: React.FC<IRadialSeparatorsProps> = ({
  count,
  style
}: IRadialSeparatorsProps) => {
  const turns = 1 / count;
  return (
    <>
      {Array.from(Array(count).keys()).map((index: number) => (
        <Separator key={index} turns={index * turns} style={style} />
      ))}
    </>
  );
};

const CircularProgressbar: React.FC<ICircularProgressbarProps> = ({
  value
}: ICircularProgressbarProps) => {
  const strokeWidth = 5;

  return (
    <CircularProgressbarWithChildren
      value={value}
      strokeWidth={strokeWidth}
      styles={buildStyles({
        strokeLinecap: 'butt',
        pathColor: '#2D3D46',
        trailColor: '#ACE1F0'
      })}>
      <RadialSeparators
        count={80}
        style={{
          background: '#040C11',
          width: '10px',
          // This needs to be equal to props.strokeWidth
          height: `${strokeWidth}%`
        }}
      />
    </CircularProgressbarWithChildren>
  );
};

export default CircularProgressbar;
