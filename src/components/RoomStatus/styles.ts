import styled, { keyframes } from 'styled-components';
import { Dialog as MuiDialog, IconButton } from '@mui/material';
import Select from '@mui/material/Select';

const spin1 = keyframes`
  0% {
      transform: rotate(0);
  }
  100% {
      transform: rotate(360deg);
  }
`;

const spin2 = keyframes`
  0% {
      transform: rotate(72deg);
  }
  100% {
      transform: rotate(-288deg);
  }
`;

const spin3 = keyframes`
  0% {
      transform: rotate(-144deg);
  }
  100% {
      transform: rotate(216deg);
  }
`;

export const Container = styled.section`
  max-width: 768px;
  max-height: 550px;
  display: flex;
  align-items: center;
  justify-content: space-around;
`;

export const RoomStatusWrapper = styled.div`
  width: 550px;
  height: 550px;
  position: relative;
  display: flex;
  flex-direction: column;
  justify-content: center;
  background: radial-gradient(circle, var(--thm-gradient-end) 0%, var(--thm-gradient-start) 43%);
  border-radius: 50%;

  > h1 {
    margin: 0;
    font-size: 37px;
    font-weight: normal;
  }
  p {
    margin: 0;
    font-size: 18px;
    :first-of-type {
      margin-bottom: 20px;
      color: var(--clr-blue);
      opacity: 0.75;
    }
    :nth-of-type(2) {
      font-size: 17px;
      color: var(--clr-blue);
      font-weight: bold;
    }
  }

  @media (max-width: 1024px) {
    width: 430px;
    height: 430px;
    > h1 {
      font-size: 30px;
    }
    > p {
      font-size: 15px;
      :nth-of-type(2) {
        font-size: 14px;
      }
    }
  }

  @media (max-width: 768px) {
    width: 290px;
    height: 290px;
    margin: 20px auto;
    > h1 {
      font-size: 20px;
    }
    > p {
      font-size: 10px;
      &:nth-of-type(2) {
        font-size: 9px;
      }
    }
  }
`;

export const CirclesWrapper = styled.div`
  height: inherit;
  width: inherit;
  position: absolute;
`;

export const Circle = styled.div`
  box-sizing: border-box;
  position: absolute;
  width: 100%;
  height: 100%;
  border: 10px solid transparent;
  border-radius: 115% 140% 145% 110%/125% 140% 110% 125%;
  mix-blend-mode: screen;
  :nth-child(1) {
    border-color: var(--clr-blue);
    transform-origin: 50% 50%;
    animation: ${spin1} 5.5s linear infinite;
  }
  :nth-child(2) {
    border-width: 25px;
    border-color: var(--clr-blue-1);
    transform-origin: 50% 50%;
    animation: ${spin2} 5.5s linear infinite;
  }
  :nth-child(3) {
    border-color: var(--clr-primary);
    transform-origin: 50% 50%;
    animation: ${spin3} 5.5s linear infinite;
  }
`;

export const ProgressBarWrapper = styled.div`
  width: 76%;
  height: 75%;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);

  & .CircularProgressbar-trail {
    stroke-width: 4.7;
  }
`;

export const BookingButton = styled.button<{ modalButton?: boolean }>`
  width: 125px;
  height: 100px;
  margin: ${({ modalButton }) => modalButton && '0 auto'};
  margin-top: ${({ modalButton }) => modalButton && '100px'};
  opacity: ${({ disabled }) => disabled && '.5'};
  position: relative;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  align-self: flex-end;
  cursor: ${({ disabled }) => (!disabled ? 'pointer' : 'unset')};
  color: var(--thm-font);
  line-height: 0.7;
  background: transparent
    linear-gradient(180deg, var(--thm-gradient-start) 0%, var(--thm-gradient-end) 100%) 0% 0%
    no-repeat padding-box;
  box-shadow: 0px 4px 10px var(--thm-clr-shadow);
  border: none;
  border-radius: 17px 0px 17px 17px;
  ::before {
    content: '';
    width: 31px;
    height: 45px;
    position: absolute;
    top: 16px;
    left: 16px;
    border: 1px solid var(--thm-clr-shadow);
    border-right: none;
    border-radius: 12px 0 0 12px;
  }
  ::after {
    content: '';
    width: 10px;
    height: 10px;
    position: absolute;
    top: 16px;
    left: 46px;
    border: 1px solid var(--clr-blue);
    border-left: none;
    border-bottom: none;
  }

  > span {
    font-size: 1.875rem !important;
    > em {
      color: var(--clr-blue);
      font-size: 2rem;
      font-style: normal;
    }
    :nth-of-type(2) {
      margin-left: 30px;
    }
  }

  @media (max-width: 1024px) {
    width: 100px;
    height: 80px;
    line-height: 0.6;
    position: relative;
    right: 7px;

    > span {
      font-size: 1.5625rem !important;
      > em {
        font-size: 1.6875rem !important;
      }
    }

    ::before {
      width: 25px;
      height: 34px;
      top: 14px;
      left: 12px;
    }
    ::after {
      width: 6px;
      left: 38px;
      top: 14px;
    }
  }
`;

export const Dialog = styled(MuiDialog)`
  > div {
    > div {
      width: 660px;
      height: 660px;
      padding: 50px;
      background: var(--clr-blue-1) 0% 0% no-repeat padding-box;
      box-sizing: border-box;
      border-radius: 26px;
    }
  }
  span {
    color: #fff;
    font-size: 1.125rem;

    @media (max-width: 1024px) {
      font-size: 1rem;
    }

    > span {
      margin-left: 5px;
    }
  }
  svg {
    color: var(--clr-blue);
    font-size: 35px;
  }

  @media (max-width: 1024px) {
    > div {
      > div {
        width: 600px;
        height: 600px;
      }
    }
    span {
      font-size: 1rem;
    }
    svg {
      font-size: 30px;
    }
  }
`;

export const TitleAndCloseButtonWrapper = styled.div`
  display: flex;
  justify-content: center;

  > h2 {
    color: #ffcf54;
    font-size: 30px;
    @media (max-width: 1024px) {
      font-size: 28px;
    }
  }
`;

export const CloseArrowButton = styled(IconButton)`
  position: absolute !important;
  left: 70px;
  top: 70px;
`;

export const RoomInfoWrapper = styled.div`
  margin-top: 90px;
  margin-left: 60px;
  display: grid;
  grid-template-columns: 50px 170px 1fr;
  grid-gap: 10px;
  align-items: center;
  text-transform: uppercase;
`;

export const DateAndDropdownWrapper = styled.div`
  margin-top: 50px;
  margin-left: 60px;
  display: grid;
  grid-template-columns: 50px 100px 120px 1fr;
  grid-gap: 12px;
  align-items: center;
`;

export const CurrentTime = styled.span`
  text-align: center;
`;

export const Dropdown = styled(Select)`
  &.MuiOutlinedInput-root {
    background-color: #fff;
    border-radius: 6px;
    width: 100px;
    height: 32px;
    font-size: 15px;
  }
  .MuiOutlinedInput-notchedOutline {
    border: none;
  }
  .MuiSvgIcon-root {
    color: #728091;
    font-size: 25px;
  }
  .MuiPaper-root {
    border-bottom-right-radius: 6px;
    border-bottom-left-radius: 6px;
    border-top-right-radius: 0;
    border-top-left-radius: 0;
  }
`;
