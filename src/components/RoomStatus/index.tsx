import { useContext, useEffect, useState } from 'react';
import { AccountInfo, IPublicClientApplication } from '@azure/msal-browser';

import UserContext from '../../store/UserContext';
import roomsConfig from '../../config/roomsConfig';
import useRoomStatus from '../../hooks/useRoomStatus';
import CircularProgressbar from './CircularProgressbar';
import NewMeetingModal from './NewMeetingModal';
import {
  Container,
  CirclesWrapper,
  Circle,
  ProgressBarWrapper,
  RoomStatusWrapper,
  BookingButton
} from './styles';
import 'react-circular-progressbar/dist/styles.css';

interface IRoomStatusProps {
  roomName: string;
  graphData: Array<object>;
  presentMoment: Date;
  createEventEndpoint: string;
  instance: IPublicClientApplication;
  accounts: AccountInfo[];
  loading: boolean;
  triggerDataRefresh: () => void;
}

const RoomStatus: React.FC<IRoomStatusProps> = ({
  roomName,
  graphData,
  presentMoment,
  createEventEndpoint,
  accounts,
  instance,
  loading,
  triggerDataRefresh
}: IRoomStatusProps) => {
  const userCtx = useContext(UserContext);
  const [isDialogOpen, setIsDialogOpen] = useState<boolean>(false);
  const { ongoingEvent, roomAvailability, progressbarValue } = useRoomStatus({
    graphData,
    presentMoment
  });

  const modalProps = {
    isOpen: isDialogOpen,
    setIsDialogOpen,
    roomName: roomName.toUpperCase(),
    roomLocation: roomsConfig[roomName].location,
    roomCapacity: `Capacity ${roomsConfig[roomName].capacity}`,
    presentMoment,
    ongoingEvent,
    handleTimeUntilNextEvent: roomAvailability.handleTimeUntilNextEvent,
    instance,
    accounts,
    createEventEndpoint,
    triggerDataRefresh
  };

  const renderBookingButton = !!(userCtx.username?.includes(roomName) || ongoingEvent.value);

  useEffect(() => {
    ongoingEvent.handleChange(graphData);
  }, [graphData]);

  return (
    <Container>
      <NewMeetingModal modalProps={modalProps} />

      <RoomStatusWrapper>
        <CirclesWrapper>
          {Array.from(Array(3).keys()).map((index: number) => (
            <Circle key={index} />
          ))}
        </CirclesWrapper>
        <h1 data-testid="heading">{roomName.toUpperCase()}</h1>
        <p>{roomsConfig[roomName].location}</p>
        {loading ? (
          <p>Loading...</p>
        ) : ongoingEvent.value ? (
          <p>Busy until {roomAvailability.handleTimeUntilFee()}</p>
        ) : (
          <p>Available until {roomAvailability.handleTimeUntilNextEvent()}</p>
        )}
        <ProgressBarWrapper>
          <CircularProgressbar value={ongoingEvent.value ? progressbarValue : 100} />
        </ProgressBarWrapper>
      </RoomStatusWrapper>
      {renderBookingButton && (
        <BookingButton
          disabled={ongoingEvent.value ? true : false}
          onClick={() => setIsDialogOpen(true)}>
          <span>
            <em>b</em>ook
          </span>
          <span>now</span>
        </BookingButton>
      )}
    </Container>
  );
};

export default RoomStatus;
