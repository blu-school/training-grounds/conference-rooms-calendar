import styled from 'styled-components';
import { Link as ReactLink } from 'react-router-dom';

export const Header = styled.header`
  margin-bottom: 0rem;
`;

export const Nav = styled.nav`
  width: 100%;
  margin-bottom: 1rem;
`;

export const Ul = styled.ul`
  padding: 0;
  margin: 0;
  list-style: none;
  display: grid;
  grid-template-columns: repeat(5, 1fr);
  gap: 1rem;

  @media (max-width: 1024px) {
    gap: 0.75rem;
  }
`;

export const Li = styled.li`
  --el-clr-shadow: var(--clr-blue);
  --el-clr-start: var(--thm-gradient-start);
  --el-clr-end: var(--thm-gradient-end);

  border-radius: 17px 0px 17px 17px;
  background: transparent linear-gradient(180deg, var(--el-clr-start) 0%, var(--el-clr-end) 100%) 0%
    0% no-repeat padding-box;
  box-shadow: 0px 6px 10px var(--el-clr-shadow);
  transition: all 300ms ease-in-out;
  padding: 0.75rem 0.5rem;

  &.active,
  &:hover {
    --el-clr-shadow: var(--clr-primary);
    --el-clr-start: var(--thm-gradient-active-primary);
    --el-clr-end: var(--thm-gradient-active-secondary);
    & > a {
      opacity: 1;
    }
  }
`;

export const Link = styled(ReactLink)`
  text-decoration: none;
  color: var(--thm-font);
  text-transform: uppercase;
  font-size: clamp(0.75rem, 2vw, 1rem);
  letter-spacing: 1px;
  opacity: 0.8;
  transition: opacity 300ms ease-in-out;
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;

  @media (max-width: 1024px) {
    font-size: 0.875rem;
  }
`;

export const StyledDate = styled.div`
  --clr-date: var(--clr-primary);
  --clr-day: var(--clr-blue);

  font-size: 1rem;
  text-align: right;
  margin-bottom: 0.25rem;
  > p {
    margin: 0;
  }
  .date {
    color: var(--clr-date);
    margin-bottom: -5px;
    top: 16px;
    left: 46px;
  }

  @media (max-width: 1024px) {
    font-size: 0.875rem;
  }
`;
