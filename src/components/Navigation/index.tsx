import roomsConfig from '../../config/roomsConfig';
import { Ul, Nav, Link, Li, Header } from './styles';
import { useLocation } from 'react-router-dom';
import DateWeekday from './DateWeekday';

const NavBar: React.VFC = () => {
  const location = useLocation();
  return (
    <Header>
      <Nav>
        <Ul className="theme-transition">
          {Object.keys(roomsConfig).map((key) => (
            <Li key={key} className={key === location.pathname.replace('/', '') ? 'active' : ''}>
              <Link to={`/${roomsConfig[key].name}`}>{roomsConfig[key].name}</Link>
            </Li>
          ))}
        </Ul>
      </Nav>
      <DateWeekday />
    </Header>
  );
};

export default NavBar;
