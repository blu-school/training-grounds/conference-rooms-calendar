import { useState } from 'react';
import useInterval from '../../hooks/useInterval';
import { StyledDate } from './styles';

function DateWeekday() {
  const [currentDate, setCurrentDate] = useState<Date>(new Date());

  const weekDays = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
  const formatDate =
    currentDate.getDate() + '.' + (currentDate.getMonth() + 1) + '.' + currentDate.getFullYear();

  useInterval(() => {
    setCurrentDate(new Date());
  }, 1000);

  return (
    <StyledDate>
      <p className="date">{formatDate}</p>
      <p>{weekDays[currentDate.getDay()]}</p>
    </StyledDate>
  );
}
export default DateWeekday;
