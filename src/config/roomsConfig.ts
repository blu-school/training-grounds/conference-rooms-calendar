import { format } from 'date-fns';

// fetching only today's events
const endpointQueryParams = `calendarView?startDateTime=${format(
  new Date(),
  'yyyy-MM-dd'
)}T00:00:00.0000000&endDateTime=${format(new Date(), 'yyyy-MM-dd')}T23:59:00.0000000`;

const roomsConfig = {
  bottleneck: {
    name: 'bottleneck',
    location: 'Floor 3.A (right)',
    eventsEndpoint: `${import.meta.env.VITE_APP_BOTTLENECK_CALENDAR_URL}/${endpointQueryParams}`,
    createEventEndpoint: `${import.meta.env.VITE_APP_BOTTLENECK_CALENDAR_URL}/events`,
    capacity: 12
  },
  endlosschleife: {
    name: 'endlosschleife',
    location: 'Floor 4.B (left)',
    eventsEndpoint: `${
      import.meta.env.VITE_APP_ENDLOSSCHLEIFE_CALENDAR_URL
    }/${endpointQueryParams}`,
    createEventEndpoint: `${import.meta.env.VITE_APP_ENDLOSSCHLEIFE_CALENDAR_URL}/events`,
    capacity: 8
  },
  nimmerland: {
    name: 'nimmerland',
    location: 'Floor 4.A (right)',
    eventsEndpoint: `${import.meta.env.VITE_APP_NIMMERLAND_CALENDAR_URL}/${endpointQueryParams}`,
    createEventEndpoint: `${import.meta.env.VITE_APP_NIMMERLAND_CALENDAR_URL}/events`,
    capacity: 8
  },
  schlupfloch: {
    name: 'schlupfloch',
    location: 'Floor 3.A (right)',
    eventsEndpoint: `${import.meta.env.VITE_APP_SCHLUPFLOCH_CALENDAR_URL}/${endpointQueryParams}`,
    createEventEndpoint: `${import.meta.env.VITE_APP_SCHLUPFLOCH_CALENDAR_URL}/events`,
    capacity: 5
  },
  untergrund: {
    name: 'untergrund',
    location: 'Floor 3.B (left)',
    eventsEndpoint: `${import.meta.env.VITE_APP_UNTERGRUND_CALENDAR_URL}/${endpointQueryParams}`,
    createEventEndpoint: `${import.meta.env.VITE_APP_UNTERGRUND_CALENDAR_URL}/events`,
    capacity: 8
  }
};

export default roomsConfig;
