import axios from 'axios';

const callMSGraphAPIEndpoint = axios.create({
  baseURL: 'https://graph.microsoft.com/v1.0'
});

export default callMSGraphAPIEndpoint;
